8 Puzzle megoldása A* algoritmussal:

Lépések:
1. Bolvassuk a kezdeti állapotot (standard bementről vagy álományból).
2. Beolvassuk a végállapotot standard bemenetről.
3. A* algoritmus:
    a. Kiszámoljuk az f számot (a „rossz helyen levő csempék száma” vagy Manhattan heurisztikát használva).
    b. Hozzáadjuk a listához és a várakozási sorhoz.
    c.Ellenőrizzük, hogy jelenlegi állapot megoldás-e.
    d. Megprobáljuk elmozdítani az üres csempét. 
       Ha lehet elmozdítjuk, az új állapotot hozzáadjuk a listához és a várakozási sorhoz.
    e. Növetkvő sorrendbe rendezzük a várakozási sort f szerint.
    f. Abba az irányba megyünk tovább aelyiknek kissebb az f száma, mert az van közelebb a megoldásoz.

 Példa bemeneti állapotr:
    2 1 3
    4 5 7
    6 0 8
    
Példa végső állapotra:
    1 2 3
    4 5 6
    7 8 ö
    
Összehsonlításképp a fent említett bemeneti és végső állapotra, „rossz helyen levő csempék száma”
heurisztikával 173 lépés a megoldás költsége, míg Manhatten távolságot használva a heurisztika 
kiszámításához csak 33.